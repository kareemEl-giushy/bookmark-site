// listen for the event
document.getElementById('form').addEventListener('submit', savebookmark);

// savebookmark function
function savebookmark (e){

	// get values form the form
	var siteName = document.getElementById('sitename'),
		siteUrl = document.getElementById("url");

	// store the values
	var bookmark = {
		name: siteName.value,
		url: siteUrl.value
	};
	// console.log(bookmarks);

	// test the local storage consipt
	/*localStorage.setItem('test', "hamada");
	console.log(localStorage.getItem("test"));
	console.log(localStorage.removeItem('test'));*/

	// use the local storage consipt
	// check if there is a bookmark or not
	if (localStorage.getItem('bookmarks') === null){
		// init an array
		var bookmarks = [];

		// push the data to the array
		bookmarks.push(bookmark);
		// console.log(bookmarks);

		// store the data in the localstorage
		localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
	}else{
		// get bookmarks from localstorage
		var bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
		// add bookmarke to the array
		bookmarks.push(bookmark);
		// store the whole data back to the local storage after modifing
		localStorage.setItem('bookmarks', JSON.stringify(bookmarks));

	}

	// re-fetch bookmarks from local storage again
	getbookmarks();

	// to not allow the default to reload the page
	e.preventDefault();
}

// on load function to get the data from the local storage
window.onload = function (){
	getbookmarks();
}

// delecte bookmark function
function deletebookmark(siteurl){
	// console.log(siteurl);
	// fetch the bookmarks from the local storage
	var bookmarks = JSON.parse(localStorage.getItem('bookmarks'));

	// find the same url
	for (var i = 0; i < bookmarks.length; i++){
		// check every url to find the right one
		if (bookmarks[i].url == siteurl){
			// delete the item with the that url
			bookmarks.splice(i, 1);

		}
	}
	// set the data to the local storage again
	localStorage.setItem('bookmarks', JSON.stringify(bookmarks));

	// re-fetch bookmarks from local storage again
	getbookmarks();
}
// fetch bookmarks form the local storage
function getbookmarks (){
	// check if there is a local storage or not first
	if (localStorage.getItem('bookmarks') !== null) {

		// get the bookmarks data from the local storage
		var bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
		// console.log(bookmarks);
		// get output id
		var results = document.getElementById('showresult');

		// displaying the data into the screen
		for (var i = 0; i < bookmarks.length; i++){
			var name = bookmarks[i].name,
				url = bookmarks[i].url;
			
			// console.log(bookmarks);
			results.innerHTML += "<div class='item'><h3>" + name + "</h3> <div><a class='btn' href='" + url + "' target='_blank'>Visit</a> <a class='btn-danger' onclick=\"deletebookmark(\'" + url + "\')\">Delete</a></div></div>";
		}
	}
}

// some flavor
// document.getElementsByTagName('input').onfocus = function (){
// 	document.getElementsByTagName('input').setAttribute('data', document.getElementsByTagName('input').getAttribute('placeholder'));
// 	document.getElementsByTagName('input').setAttribute('placeholder', '');
// }
// document.getElementsByTagName('input').onblur = function (){
// 	document.getElementsByTagName('input').setAttribute('placeholder', document.getElementsByTagName('input').getAttribute('data'));
// 	document.getElementsByTagName('input').setAttribute('data', '');
// }